A React and Redux Starter kit built from FacebookIncubator's Create-React-App kit 
                       so I don't have to start from scratch every time.               

It includes the following libraries on top of React starter kit:

 Redux (duh!)
 
 React-Redux
 
 Glamourous (for component styling)
 
 React-Router (latest: v4 at the time of writing this)
