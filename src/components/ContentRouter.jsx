import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import glamorous from 'glamorous';
import NoMatch from './NoMatch';
import HomeComponent from './HomeComponent';
import MessagesComponent from './MessagesComponent';

const Content = glamorous.div({
  marginTop: 60,
});

const ContentRouter = () => (
  <Content>
    <Switch>
      <Route path="/" exact component={HomeComponent} />
      <Route path="/messages" exact component={MessagesComponent} />
      <Route component={NoMatch} />
    </Switch>
  </Content>
);

export default ContentRouter;
