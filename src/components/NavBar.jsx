import React from 'react';
import glamorous from 'glamorous';

const NavBarWrapper = glamorous.div({
  width: '100%',
  height: '60px',
  position: 'fixed',
  left: 0,
  right: 0,
  top: 0,
  background: 'lightseagreen',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const StyledLink = glamorous.a({
  fontSize: '1.5rem',
  fontWeight: 200,
  margin: '0 10px',
  color: 'white',
  textDecoration: 'none',
});
const NavBar = () => (
  <NavBarWrapper>
    <StyledLink href={'/'}>
      Ishan's App
    </StyledLink>
    <StyledLink href={'/messages'}>
      Messages
    </StyledLink>
  </NavBarWrapper>
);

export default NavBar;

