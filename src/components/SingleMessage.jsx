import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';

const MessageWrapper = glamorous.div({
  width: '80%',
  padding: 20,
  margin: '15px auto',
  fontSize: '1.5rem',
  fontWeight: 200,
  border: '0.3pt solid lightgrey',
});

const SingleMessage = (props) => (
  <MessageWrapper>
    <h4>
      From Supervisor: { ' ' + props.message.supervisorId }
    </h4>
    <h4>
      To Recipient: { ' ' + props.message.recipientId }
    </h4>
    <p>
      Message: { ' ' + props.message.message }
    </p>
  </MessageWrapper>
);

SingleMessage.propTypes = {
  message: PropTypes.object,
};

export default SingleMessage;
