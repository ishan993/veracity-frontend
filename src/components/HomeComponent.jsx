import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { fetchSupervisors, fetchRecipients, sendMessage } from '../actions';

const BodyWrapper = glamorous.div({
  width: '80%',
  margin: 'auto',
  fontSize: '1.5rem',
});

const StyledSelect = glamorous(Select)({
  margin: 15,
});

const RowWrapper = glamorous.div({
  width: '95%',
  margin: '15px auto',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
});

const SubmitButton = glamorous.button({
  width: 250,
  padding: 10,
  background: 'lightseagreen',
  fontSize: '1.5rem',
  fontWeight: 200,
  color: 'white',
  outline: 'none',
  border: 0,
});

const StyledTextArea = glamorous.textarea({
  width: '100%',
  margin: 'auto',
  fontSize: '1.2rem',
  border: '0.3pt solid lightgrey',
});
class HomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      selectedRecipient: '',
      selectedSupervisor: '',
      messageValue: '',
    });
  }

  componentWillMount() {
    this.props.fetchSupervisors();
    this.props.fetchRecipients();
  }

  selectSupervisor(val) {
    this.setState({ selectedSupervisor: val.id });
  }

  selectRecipient(val) {
    this.setState({ selectedRecipient: val.id });
  }
  handleMessageChange(event) {
    this.setState({ messageValue: event.target.value });
  }
  handleSubmit() {
    console.log('trying to handle form!');
    // I wouldn't have done it this way but I didn't have enough time!
    // Who likes alert boxes anyways?
    this.props.sendMessage({
      supervisorId: this.state.selectedSupervisor,
      recipientId: this.state.selectedRecipient,
      message: this.state.messageValue,
    }).then((response) => {
      alert(response);
      this.setState({
        selectedSupervisor: '',
        selectedRecipient: '',
        messageValue: '',
      });
    });
  }
  render() {
    return (
      <BodyWrapper>
        Hello world!
        <form>
          <StyledSelect
            name="supervisorId"
            value={this.state.selectedSupervisor}
            valueKey="id"
            labelKey="name"
            options={this.props.supervisorOptions}
            onChange={val => this.selectSupervisor(val)}
            isRequired
          />
          <StyledSelect
            name="recipientId"
            valueKey="id"
            labelKey="name"
            value={this.state.selectedRecipient}
            options={this.props.recipientOptions}
            onChange={val => this.selectRecipient(val)}
            isRequired
          />
          <RowWrapper>
            <StyledTextArea
              name="message"
              value={this.state.messageValue}
              placeholder="Enter Message here!"
              rows="2"
              onChange={event => this.handleMessageChange(event)}
              required
            />
          </RowWrapper>
          <RowWrapper>
            <SubmitButton
              type="submit"
              onClick={(event) => {
                event.preventDefault();
                if (this.state.selectedRecipient === '' ||
                  this.state.selectedSupervisor === ''  ||
                  this.state.messageValue === '') {
                  alert('No empty fields allowed!');
                } else {
                  this.handleSubmit();
                }
              }}
            >
              Submit Message!
            </SubmitButton>
          </RowWrapper>
        </form>
      </BodyWrapper>
    );
  }
}

HomeComponent.propTypes = {
  supervisorOptions: PropTypes.array.isRequired,
  recipientOptions: PropTypes.array.isRequired,
  fetchSupervisors: PropTypes.func.isRequired,
  fetchRecipients: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  supervisorOptions: state.formProps.supervisorOptions,
  recipientOptions: state.formProps.recipientOptions,
});

export default connect(mapStateToProps, {
  fetchSupervisors, fetchRecipients, sendMessage,
})(HomeComponent);
