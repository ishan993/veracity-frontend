import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SingleMessage from './SingleMessage';
import { fetchMessages } from '../actions';

class MessagesComponent extends Component {
  componentWillMount() {
    this.props.fetchMessages();
  }

  render() {
    return (
      <div>
        <h1>
          Messages:
        </h1>
        {this.props.messages ?
          this.props.messages.map(message => <SingleMessage message={message} />) : ''
        }
      </div>
    );
  }
}
const mapStateToProps = state => ({
  messages: state.formProps.messages,
});

MessagesComponent.propTypes = {
  messages: PropTypes.array,
  fetchMessages: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { fetchMessages })(MessagesComponent);
