import { combineReducers } from 'redux';
import FormReducer from './form_reducer';

const rootReducer = combineReducers({
  formProps: FormReducer,
});

export default rootReducer;
