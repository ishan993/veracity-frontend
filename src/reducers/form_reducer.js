import { FETCH_MESSAGES, FETCH_SUPERVISORS, FETCH_RECIPIENTS } from '../actions';

const DEFAULT_STATE = {
  supervisorOptions: [],
  recipientOptions: [],
  allMessages: [],
};

export default (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case FETCH_SUPERVISORS:
      console.log('HITTING_FETCH_SUPERVISOR_REDUCER');
      return { ...state, supervisorOptions: action.payload };
    case FETCH_RECIPIENTS:
      return { ...state, recipientOptions: action.payload };
    case FETCH_MESSAGES:
      return { ...state, messages: action.payload };
    default:
      return state;
  }
};
