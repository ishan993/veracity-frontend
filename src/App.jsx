import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import ContentRouter from './components/ContentRouter';
import NavBar from './components/NavBar';


const App = () => (
  <div>
    <NavBar />
    <BrowserRouter>
      <ContentRouter />
    </BrowserRouter>
  </div>
);

export default App;
