// Generally, in a big application, I'd break down actions on the basis of functionality.
// Since this is a relatively small app, grouping together
// all the actions shouldn't cause much confusion.
import axios from 'axios';
import { serverConfig } from '../config';

export const FETCH_SUPERVISORS = 'FETCH_SUPERVISORS';
export const FETCH_RECIPIENTS = 'FETCH_RECIPIENTS';
export const FETCH_MESSAGES = 'FETCH_MESSAGES';
export const SEND_MESSAGE = 'SEND_MESSAGE';

export const fetchSupervisors = () => {
  console.log('HITTING_FETCH_SUPERVISORS');
  const URL = serverConfig.ROOT_URL + '/supervisors';
  const fetchSupervisorReq = axios.get(URL);

  return (dispatch) => {
    return fetchSupervisorReq.then((response) => {
      dispatch({
        type: FETCH_SUPERVISORS,
        payload: response.data.result,
      });
    }).catch((error) => {
      console.log('FETCH_SUPERVISORS_ERROR: ' + JSON.stringify(error));
    });
  };
};

export const fetchRecipients = () => {
  console.log('HITTING_FETCH_RECIPIETS');
  const URL = serverConfig.ROOT_URL + '/recipients';
  const fetchRecipientReq = axios.get(URL);

  return (dispatch) => {
    return fetchRecipientReq.then((response) => {
      dispatch({
        type: FETCH_RECIPIENTS,
        payload: response.data.result,
      });
    }).catch((error) => {
      console.log('FETCH_SUPERVISORS_ERROR: ' + JSON.stringify(error));
    });
  };
};


export const fetchMessages = () => {
  console.log('HITTING_FETCH_MESSAGES');
  const URL = serverConfig.ROOT_URL + '/messages';
  const fetchMessageReq = axios.get(URL);

  return (dispatch) => {
    return fetchMessageReq.then((response) => {
      console.log('FETCH_MESSAGES_SUCCESS: ' + JSON.stringify(response.data.result));
      dispatch({
        type: FETCH_MESSAGES,
        payload: response.data.result,
      });
    }).catch((error) => {
      console.log('FETCH_SUPERVISORS_ERROR: ' + JSON.stringify(error));
    });
  };
};

export const sendMessage = (messageObj) => {
  const URL = serverConfig.ROOT_URL + '/messages';
  const sendMessageReq = axios.post(URL, messageObj);

  return () => {
    return sendMessageReq.then((response) => {
      return ('Message sent!');
    }).catch((error) => {
      return ('Error: ' + error.response.data.message);
    });
  };
};
